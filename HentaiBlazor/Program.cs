
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Web;
using AntDesign.ProLayout;
using Microsoft.EntityFrameworkCore;
using HentaiBlazor.Data;
using HentaiBlazor.Services;
using HentaiBlazor.Services.Basic;
using HentaiBlazor.Services.Comic;
using HentaiBlazor.Services.Security;
using HentaiBlazor.Services.Anime;


var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.AddControllers();
builder.Services.AddRazorPages();
builder.Services.AddServerSideBlazor();
builder.Services.AddAntDesign();
// builder.Services.AddSingleton<WeatherForecastService>();

builder.Services.AddDbContextFactory<HentaiContext>(options =>
{
    Console.WriteLine("创建数据源工厂:");
    options.UseSqlite($"Data Source ={HentaiContext.HentaiDb}.db");
});

builder.Services.AddScoped<FunctionService>();
builder.Services.AddScoped<UserService>();

builder.Services.AddScoped<AuthorService>();
builder.Services.AddScoped<ProducerService>();
builder.Services.AddScoped<TagService>();
builder.Services.AddScoped<CatalogService>();
builder.Services.AddScoped<CryptoService>();

builder.Services.AddScoped<BookService>();
builder.Services.AddScoped<BookTagService>();
builder.Services.AddScoped<VideoService>();
builder.Services.AddScoped<VideoTagService>();

builder.Services.AddSingleton<CoverService>();
builder.Services.AddScoped<ReaderService>();
builder.Services.AddSingleton<PreviewService>();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment())
{
    app.UseExceptionHandler("/Error");
    // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
    app.UseHsts();
}

// app.UseMvc();

app.UseHttpsRedirection();

app.UseStaticFiles();

app.UseRouting();

app.MapBlazorHub();
app.MapFallbackToPage("/_Host");

app.Run();
